'use strict';

const gulp = require('gulp');
const browser = require('browser-sync').create();
const gulpLib = require('./gulp-lib');
const isProd = gulpLib.isProduction;
const config = gulpLib.config;

gulp.task('build',
	gulp.series(
		'clean',
		gulp.parallel('pages', 'styles', 'webpack', 'fonts', 'assets', 'images'))
	);

gulp.task('default',
	gulp.series(
		'build', 
		gulp.parallel(server, watch)
		)
	);

function server(done) {
	browser.init({
		proxy: 'test.loc',
		notify: false
	});
	browser.watch('public_html/**/*.*').on('change', browser.reload);
	done();
};

function watch() {
	gulp.watch(config.assets.dev, gulp.series('assets'));
	gulp.watch(config.pages.dev, gulp.series('pages'));
	gulp.watch(config.pages.watch, gulp.series('resetPages', 'pages'));
	gulp.watch(config.styles.watch, gulp.series('styles'));
	gulp.watch(config.images.dev, gulp.series('images'));
	gulp.watch(config.fonts.dev, gulp.series('fonts'));
}
