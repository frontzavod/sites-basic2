var fs = require('fs');

module.exports = function (dir){
	return fs.readdirSync(dir).map((fileName) => fileName.split('.')[0]);
};
