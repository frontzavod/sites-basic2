'use strict';

const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const gulpLib = require('../index');
const paths = gulpLib.path('images');

module.exports = function() {
	return gulp.src(paths.dev, {since: gulp.lastRun('images')})
	.pipe($.newer(paths.dist))
	.pipe($.if(gulpLib.isProduction, $.imagemin({
		progressive: true
	})))
	.pipe(gulp.dest(paths.dist));
}