'use strict';

const config = require('../config')
const rimraf = require('rimraf');

module.exports = function(done) {
	rimraf(config.root.dist, done);
}