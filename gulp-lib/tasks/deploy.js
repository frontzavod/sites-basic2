'use strict';

const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const ftp = require('vinyl-ftp');
const fs = require('vinyl-fs');
const inquirer = require('inquirer');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const config = require('../index').path('deploy');
const folderToPush = require('../index').config.root.dist + '/**/*';


let question1 = {
	type: 'list',
	name: 'version',
	message: 'Выбери версию, куда загружать. DEVELOPMENT или PRODUCTION',
	choices: [
	{name: 'DEVELOPMENT', value:'dev'},
	new inquirer.Separator(),
	{name: 'PRODUCTION', value: 'prod'}
	],
	default: 0
}


module.exports = function(done) {
	inquirer.prompt([question1]).then((answers) => {
		inquirer.prompt([{
			type: 'confirm',
			name: 'confirm',
			message: `Загрузить проект на ${config[answers.version].path}?`,
		}]).then((answers) => {
			let ftpConfig = {
				host: config[answers.version].host,
				user: config[answers.version].user,
				pass: config[answers.version].password,
				parallel: 10
			}
			let conn = ftp.create(ftpConfig);
			fs.src([folderToPush], {buffer: false})
			.pipe(conn.dest(config[answers.version].path));
			done();
		})
	})
}