'use strict';

const $ = require('gulp-load-plugins')();
const gulp = require('gulp');
const named = require('vinyl-named');
const gulplog = require('gulplog');
const webpackStream = require('webpack-stream');
const webpack = webpackStream.webpack;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const gulpLib = require('../index');
const paths = gulpLib.path('js');
const path = require('path');

function getAbsPaths(paths) {
	return paths.map((str) => path.join(__dirname,'../../',str));
}
let babelIncludes = getAbsPaths(paths.babelIncludes)

let webpackConfig = {
	output: {
		publicPath: paths.publicPath,
	},
	module: {
		loaders: [{
			test:    /\.js$/,
			include: babelIncludes,
			loader:  'babel-loader?presets[]=env'
		}]
	},
	plugins: [
	new webpack.NoErrorsPlugin(),
	new webpack.ProvidePlugin({
		$: 'jquery',
		jQuery: 'jquery'
	}),
	new webpack.optimize.CommonsChunkPlugin({
		name: 'common'
	})
	]
};

if (!gulpLib.isProduction) {
	webpackConfig.watch = true;
	webpackConfig.devtool = 'cheap-module-inline-source-map'
} else {
	webpackConfig.watch = false;
	webpackConfig.devtool = null;
	webpackConfig.plugins.push(new UglifyJsPlugin())
}

module.exports = function(callback) {
	let firstBuildReady = false;

	function done(err, stats) {
		firstBuildReady = true;
		if (err) {
			return;
		}
		gulplog[stats.hasErrors() ? 'error' : 'info'](stats.toString({
			assets: true,
			chunks: false,
			chunkModules: false,
			colors: true,
			hash: false,
			timings: true,
			version: false
		}));
	}

	return gulp.src(paths.dev)
	.pipe($.plumber({
		errorHandler: $.notify.onError(err => ({
			title:   'Webpack',
			message: err.message
		}))
	}))
	.pipe(named())
	.pipe(webpackStream(webpackConfig, null, done))
	.pipe(gulp.dest(paths.dist))
	.on('data', () => firstBuildReady && callback())
};