'use strict';

const gulp = require('gulp');
const paths = require('../index').path('fonts');
const $ = require('gulp-load-plugins')();

module.exports = function() {
	return gulp.src(paths.dev, {since: gulp.lastRun('fonts')})
	.pipe($.newer(paths.dist))
	.pipe(gulp.dest(paths.dist));
}