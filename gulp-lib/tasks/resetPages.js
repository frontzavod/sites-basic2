'use strict';

const panini = require('panini');

module.exports = function(done) {
	panini.refresh();
	done();
}