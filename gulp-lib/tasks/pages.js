'use strict';

const gulp = require('gulp');
const panini = require('panini');
const paths = require('../index').path('pages');

module.exports = function() {
	return gulp.src(paths.dev)
	.pipe(panini(paths.panini))
	.pipe(gulp.dest(paths.dist));
}