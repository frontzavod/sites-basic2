'use strict';

const gulp = require('gulp');
const paths = require('../index').path('assets');
const $ = require('gulp-load-plugins')();

module.exports = function() {
	return gulp.src(paths.dev, {since: gulp.lastRun('assets')})
	.pipe($.newer(paths.dist))
	.pipe(gulp.dest(paths.dist));
}