'use strict';

const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const gulpLib = require('../index');
const isProd = gulpLib.isProduction;
const paths = gulpLib.path('styles');

module.exports = function() {
	return gulp.src(paths.dev)
	.pipe($.if(!isProd, $.sourcemaps.init()))
	.pipe($.sass({
		includePaths: paths.pathIncludes
	})
	.on('error', $.sass.logError))
	.pipe($.autoprefixer({
		browsers: ["last 2 versions", "ie >= 9", "ios >= 7"]
	}))
	.pipe($.if(isProd, $.cleanCss({ compatibility: 'ie9' })))
	.pipe($.if(!isProd, $.sourcemaps.write()))
	.pipe(gulp.dest(paths.dist))
}