'use strict';

const gulp = require('gulp');
const tasks = require('./getTaskNames')(__dirname+'/tasks');

function lazyRequireTask(taskName) {
	gulp.task(taskName, function(callback) {
		let task = require(`./tasks/${taskName}`);
		return task(callback);
	})
}

module.exports = tasks.map((task) => lazyRequireTask(task))