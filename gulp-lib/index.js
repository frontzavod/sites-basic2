const yargs = require('yargs');
const config = require('./config');

const tasks = require('./createTasks');

module.exports = {
	isProduction: !!(yargs.argv.production),
	config: config,
	path: task => config[task]
}
